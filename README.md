## Usage
```
npm install

npm run serve
```

## 텍스트

### 영어 작성시
/src/locales/en.json
```
{
  "message1": "English Text 1", 
  "message2": "English Text 2", 
  "message3": "English Text 3", 
  "message4": "English Text 4", 
  "message5": "English Text 5"
}
```

### 한국어 작성시
/src/locales/ko.json
```
{
  "message1": "한글 텍스트 1", 
  "message2": "한글 텍스트 2", 
  "message3": "한글 텍스트 3", 
  "message4": "한글 텍스트 4", 
  "message5": "한글 텍스트 5"
}
```


## 이미지
빌드시 경로 변경 문제로 public 폴더 이용
- 영어 
-  public/en/logo.png

- 한국어 
- public/ko/logo.png

